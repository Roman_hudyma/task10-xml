<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body style="font-family: Arial; font-size: 12pt; background-color: #EEE">
                <div style="background-color: gray; color: black;">
                    <h2>D E V I C E S</h2>
                </div>
                <table border="3">
                    <tr bgcolor="#5E85CE">
                        <th>Gun ID</th>
                        <th>Model</th>
                        <th>Handy</th>
                        <th>Origin</th>
                        <th>Materials</th>
                        <th>carry</th>
                        <th>sightingRange</th>
                        <th>presenceOfTheClamp</th>
                        <th>availabilityOptics</th>
                    </tr>

                    <xsl:for-each select="guns/gun">
                        <tr bgcolor="#D59069">
                            <td >
                                <xsl:value-of select="@gunID"/>
                            </td>
                            <td>
                                <xsl:value-of select="model"/>
                            </td>
                            <td>
                                <xsl:value-of select="handy"/>
                            </td>
                            <td>
                                <xsl:value-of select="origin"/>
                            </td>
                            <td>
                                <xsl:for-each select="materials/material">
                                    <xsl:value-of select="."/>
                                    <xsl:text>, </xsl:text>
                                </xsl:for-each>
                            </td>
                            <td>
                                <xsl:value-of select="TTSes/carry"/>
                            </td>
                            <td>
                                <xsl:value-of select="TTSes/sightingRange"/>
                            </td>
                            <td>
                                <xsl:choose>
                                    <xsl:when test="TTSes/presenceOfTheClamp='true'">
                                        <xsl:text>YES</xsl:text>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:text>NO</xsl:text>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>
                            <td>
                                <xsl:choose>
                                    <xsl:when test="TTSes/availabilityOptics='true'">
                                        <xsl:text>YES</xsl:text>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:text>NO</xsl:text>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>

                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>