package com.epam.controller.converter;

import com.codeborne.selenide.Selenide;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

import static com.codeborne.selenide.Selenide.open;

public class XmlToHtmlConverter {
    private static final Logger log = LogManager.getLogger();
    public static void run() {
        Source xml = new StreamSource(new File("src\\main\\resources\\xml\\guns.xml"));
        Source xslt = new StreamSource("src\\main\\resources\\xml\\gunsXSL.xsl");

        convertXMLToHTML(xml, xslt);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        open("http://localhost:63342/XML/Guns/xml/guns.html?_ijt=dk8d6h2su9q7pk7p4m3brtfpbt");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Selenide.close();
    }

    public static void convertXMLToHTML(Source xml, Source xslt) {
        StringWriter sw = new StringWriter();

        try {

            FileWriter fw = new FileWriter("src\\main\\resources\\xml\\guns.html");
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transform = tFactory.newTransformer(xslt);

            transform.transform(xml, new StreamResult(sw));
            fw.write(sw.toString());
            fw.close();



        } catch (IOException | TransformerFactoryConfigurationError | TransformerException e) {
            e.printStackTrace();
        }
        log.info("Successfully converted to HTML");
    }
}
