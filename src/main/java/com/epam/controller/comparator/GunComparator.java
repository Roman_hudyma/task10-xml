package com.epam.controller.comparator;

import com.epam.model.Gun;

import java.util.Comparator;

public class GunComparator implements Comparator<Gun> {
    @Override
    public int compare(Gun o1, Gun o2) {
        return o1.getGunID()-o2.getGunID();
    }
}
