package com.epam.controller.parser.sax;

import com.epam.model.Gun;
import com.epam.model.validator.XmlValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SaxParser {
  private static final Logger log = LogManager.getLogger();
  private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

  public static List<Gun> parse(File xml, File xsd) {
    List<Gun> guns = new ArrayList<>();
    try {
      saxParserFactory.setSchema(XmlValidator.createSchema(xsd));
      saxParserFactory.setValidating(true);

      SAXParser saxParser = saxParserFactory.newSAXParser();
      System.out.println(saxParser.isValidating());
      SaxHandler saxHandler = new SaxHandler();
      saxParser.parse(xml, saxHandler);
      guns = saxHandler.getDeviceList();
    } catch (SAXException | ParserConfigurationException | IOException ex) {
      ex.printStackTrace();
    }
    return guns;
  }
}