package com.epam.controller.parser.sax;

import com.epam.model.Gun;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.List;

public class SaxRunner {
  private static final Logger log = LogManager.getLogger();
  public void run() {
    File xmlFile = new File("src\\main\\resources\\xml\\guns.xml");
    File xsdFile = new File("src\\main\\resources\\xml\\gunsXSD.xsd");

    List<Gun> devices = SaxParser.parse(xmlFile, xsdFile);
    System.out.println(devices);
    log.info("Successfully parsed");

  }

}
