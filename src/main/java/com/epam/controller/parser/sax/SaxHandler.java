package com.epam.controller.parser.sax;
import com.epam.model.Gun;
import com.epam.model.Material;
import com.epam.model.TacticalSpecifications;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SaxHandler extends DefaultHandler {

  private static final String GUN_TAG = "gun";
  private static final String MODEL_TAG = "model";
  private static final String ORIGIN_TAG = "origin";
  private static final String HANDY_TAG = "price";
  private static final String TTSES_tag = "TTSes";
  private static final String MATERIALS_TAG = "materials";
  private static final String CARRY_TAG="carry";
  private static final String SIGHTINGRANGE_TAG="sightingRange";
  private static final String PRESENCEOFTHECLAMP_tag="presenceOfTheClamp";
  private static final String AVAILABILITYOPTICS ="availabilityOptics";


  private static final String GUN_ID_ATTRIBUTE = "gunID";

  private List<Gun> guns = new ArrayList<>();
  private Gun gun;
  private TacticalSpecifications specifications;
  private List<Material> materials;
  private String currentElement;

  public List<Gun> getDeviceList() {
    return this.guns;
  }

  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes)
      throws SAXException {
    currentElement = qName;

    switch (currentElement) {
      case GUN_TAG: {
        String deviceId = attributes.getValue(GUN_ID_ATTRIBUTE);
        gun = new Gun();
        gun.setGunID(Integer.parseInt(deviceId));
        break;
      }
      case TTSES_tag:
      {
        specifications=new TacticalSpecifications();
        break;
      }
      case MATERIALS_TAG: {
        materials = new ArrayList<>();
        break;
      }
    }
  }

  @Override
  public void endElement(String uri, String localName, String qName) throws SAXException {
    switch (qName) {
      case GUN_TAG: {
        guns.add(gun);
        break;
      }
      case TTSES_tag:{
        gun.setSpecifications(specifications);
        specifications=null;
        break;
      }
      case MATERIALS_TAG: {
        gun.setMaterials(materials);
        materials = null;
        break;
      }

      }
    }

  @Override
  public void characters(char[] ch, int start, int length) throws SAXException {
    if (currentElement.equals(MODEL_TAG)) {
      gun.setModel(new String(ch, start, length));
    }
    if (currentElement.equals(HANDY_TAG)) {
      gun.setHandy(new String(ch, start, length));
    }
    if (currentElement.equals(ORIGIN_TAG)) {
      gun.setOrigin(new String(ch, start, length));
    }
    if(currentElement.equals(MATERIALS_TAG))
    {
      Material material =new Material(new String(ch,start,length));
      materials.add(material);
    }
    if(currentElement.equals(CARRY_TAG)){
      specifications.setCarry(new String(ch, start, length));
    }
    if(currentElement.equals(SIGHTINGRANGE_TAG)){
      specifications.setSightingRange(new String(ch, start, length));
    }
    if(currentElement.equals(PRESENCEOFTHECLAMP_tag)){
      specifications.setPresenceOfTheClamp(Boolean.parseBoolean(new String(ch, start, length)));
    }
    if(currentElement.equals(AVAILABILITYOPTICS)){
      specifications.setAvailabilityOptics(Boolean.parseBoolean(new String(ch, start, length)));
    }
  }
  }

