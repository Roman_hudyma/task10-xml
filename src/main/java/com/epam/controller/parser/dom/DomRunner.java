package com.epam.controller.parser.dom;

import com.epam.model.Gun;
import com.epam.model.validator.XmlValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class DomRunner {
    private static final Logger log = LogManager.getLogger();
    private static DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    private static DocumentBuilder documentBuilder;

    public static void run() {
        File xmlFile = new File("src\\main\\resources\\xml\\guns.xml");
        File xsdFile = new File("src\\main\\resources\\xml\\gunsXSD.xsd");

        Document document = null;
        try {
            documentBuilder = factory.newDocumentBuilder();
            document = documentBuilder.parse(xmlFile);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

        if (XmlValidator.validate(document, xsdFile)) {
            List<Gun> deviceList = DomParser.parse(document);
            System.out.println(deviceList);
        } else {
            log.info("XML document failed validation.");
        }
        log.info("Successfully parsed");
    }

}

