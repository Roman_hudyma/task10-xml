package com.epam.controller.parser.dom;

import com.epam.model.Gun;
import com.epam.model.Material;
import com.epam.model.TacticalSpecifications;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DomParser {

    public static List<Gun> parse(Document document) {
        List<Gun> guns = new ArrayList<Gun>();
        NodeList nodeList = document.getElementsByTagName("gun");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Gun gun = new Gun();
            Material material;
            TacticalSpecifications specifications;
            Node node = nodeList.item(i);
            Element element = (Element) node;
            gun.setGunID(Integer.parseInt(element.getAttribute("gunID")));
            gun.setModel(element.getElementsByTagName("model").item(0).getTextContent());
            gun.setHandy(element.getElementsByTagName("handy").item(0).getTextContent());

            gun.setOrigin(element.getElementsByTagName("origin").item(0).getTextContent());
            specifications=getTacticalSpecificatio(element.getElementsByTagName("TTSes"));
            gun.setSpecifications(specifications);
            guns.add(gun);
        }
        return guns;
    }

    private static TacticalSpecifications getTacticalSpecificatio(NodeList nodeList) {
        TacticalSpecifications specifications = new TacticalSpecifications();
        if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodeList.item(0);
            specifications.setCarry(element
                    .getElementsByTagName("carry")
                    .item(0)
                    .getTextContent());
            specifications.setSightingRange(element
                    .getElementsByTagName("sightingRange")
                    .item(0)
                    .getTextContent());
            specifications.setPresenceOfTheClamp(Boolean.parseBoolean(element.getElementsByTagName("presenceOfTheClamp")
                    .item(0).getTextContent()));
            specifications.setAvailabilityOptics(Boolean.parseBoolean(element.getElementsByTagName("availabilityOptics")
                    .item(0).getTextContent()));

        }
        return specifications;
    }

    private static List<Material> getMaterial(NodeList nodeList) {
        List<Material> materials = new ArrayList<Material>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Material material = new Material();
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                material.setMaterial(element.getTextContent());
                materials.add(material);
            }
        }
        return materials;
    }
}

