package com.epam.controller.parser.stax;


import com.epam.model.Gun;
import com.epam.model.Material;
import com.epam.model.TacticalSpecifications;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class StaxParser {

    private static final String GUN_TAG = "gun";
    private static final String MODEL_TAG = "model";
    private static final String ORIGIN_TAG = "origin";
    private static final String HANDY_TAG = "price";
    private static final String TTSES_tag = "TTSes";
    private static final String MATERIALS_TAG = "materials";
    private static final String CARRY_TAG = "carry";
    private static final String SIGHTINGRANGE_TAG = "sightingRange";
    private static final String PRESENCEOFTHECLAMP_tag = "presenceOfTheClamp";
    private static final String AVAILABILITYOPTICS = "availabilityOptics";


    private static final String GUN_ID_ATTRIBUTE = "gunID";

    private XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

    private List<Gun> guns = new ArrayList<>();
    private Gun gun;
    private TacticalSpecifications specifications;

    private List<Material> materials;

    public List<Gun> parse(File xml) {
        try {
            XMLEventReader xmlEventReader = xmlInputFactory
                    .createXMLEventReader(new FileInputStream(xml));
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name) {
                        case GUN_TAG: {
                            gun = new Gun();
                            Attribute gunId = startElement.getAttributeByName(new QName(GUN_ID_ATTRIBUTE));
                            if (gunId != null) {
                                gun.setGunID(Integer.parseInt(gunId.getValue()));
                            }
                            break;
                        }
                        case MODEL_TAG: {
                            xmlEvent = xmlEventReader.nextEvent();
                            gun.setModel(xmlEvent.asCharacters().getData());
                            break;
                        }

                        case HANDY_TAG: {
                            xmlEvent = xmlEventReader.nextEvent();
                            gun.setHandy(xmlEvent.asCharacters().getData());
                            break;
                        }
                        case ORIGIN_TAG: {
                            xmlEvent = xmlEventReader.nextEvent();
                            gun.setOrigin(xmlEvent.asCharacters().getData());
                            break;
                        }
                        case MATERIALS_TAG: {
                            materials = new ArrayList<>();
                            break;
                        }
                        case TTSES_tag:
                            specifications = new TacticalSpecifications();
                            break;
                        case CARRY_TAG: {
                            xmlEvent = xmlEventReader.nextEvent();
                            specifications.setCarry(xmlEvent.asCharacters().getData());
                            break;
                        }
                        case SIGHTINGRANGE_TAG: {
                            xmlEvent = xmlEventReader.nextEvent();
                            specifications.setSightingRange(xmlEvent.asCharacters().getData());
                            break;
                        }
                        case PRESENCEOFTHECLAMP_tag: {
                            xmlEvent = xmlEventReader.nextEvent();
                            specifications.setPresenceOfTheClamp(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                            break;
                        }
                        case AVAILABILITYOPTICS: {
                            xmlEvent = xmlEventReader.nextEvent();
                            specifications.setAvailabilityOptics(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                            break;
                        }

                    }
                }

                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    String name = endElement.getName().getLocalPart();
                    switch (name) {
                        case GUN_TAG: {
                            guns.add(gun);
                            gun = null;
                            break;
                        }
                        case MATERIALS_TAG: {
                            gun.setMaterials(materials);
                            materials = null;
                            break;
                        }
                        case TTSES_tag: {
                            gun.setSpecifications(specifications);
                            specifications = null;
                            break;
                        }
                    }
                }
            }
        } catch (XMLStreamException | FileNotFoundException e) {
            e.printStackTrace();
        }
        return guns;
    }
}

