package com.epam.controller.parser.stax;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

public class StaxRunner {
  private static final Logger log = LogManager.getLogger();

  public void run() {

    File xmlFile = new File("src\\main\\resources\\xml\\guns.xml");
  File xsdFile = new File("src\\main\\resources\\xml\\gunsXSD.xsd");
  StaxParser staxParser = new StaxParser();
    System.out.println(staxParser.parse(xmlFile));
    log.info("Successfully parsed");
}

}
