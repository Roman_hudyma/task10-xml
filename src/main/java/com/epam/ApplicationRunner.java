package com.epam;

import com.epam.view.MainMenu;

public class ApplicationRunner {
    public static void main(String[] args) {
        new MainMenu().run();
    }
}
