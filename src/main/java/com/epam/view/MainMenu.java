package com.epam.view;

import com.epam.controller.converter.XmlToHtmlConverter;
import com.epam.controller.parser.dom.DomRunner;
import com.epam.controller.parser.sax.SaxRunner;
import com.epam.controller.parser.stax.StaxRunner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;

public class MainMenu {

    private static final Logger logger = LogManager.getLogger();
    private static List<String> menu = new ArrayList<>();
    private static ResourceBundle messages = ResourceBundle.getBundle("command");
    private static final Scanner scanner = new Scanner(System.in);
    private int choose;

    public void run() {
        createMenu();
        do {
            for (String s : menu) {
                System.out.println(s);
            }
            choose = scanner.nextInt();
            switch (choose) {
                case 1:
                    new DomRunner().run();
                    break;
                case 2:
                    new SaxRunner().run();
                    break;
                case 3:
                    new StaxRunner().run();
                    break;
                case 4:
                    new XmlToHtmlConverter().run();
                    break;
            }
        }
        while (!(choose == 5));
    }


    private static void createMenu() {
        menu.add((messages.getString("command.1")));
        menu.add((messages.getString("command.2")));
        menu.add((messages.getString("command.3")));
        menu.add((messages.getString("command.4")));


    }
}
